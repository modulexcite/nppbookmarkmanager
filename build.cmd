@echo off
cls
call clean.cmd

SetLocal

set BUILD_TYPE=%1
if "%BUILD_TYPE%" == "" (
  set BUILD_TYPE=DEBUG
)

echo -- Build Type: %BUILD_TYPE%

set GENERATOR=%2
if "%GENERATOR%" == "" (
  set GENERATOR=MSYS Makefiles
)

echo -- Generator: %GENERATOR%

cmake -D CMAKE_BUILD_TYPE:String=%BUILD_TYPE% -G "%GENERATOR%" CMakeLists.txt

EndLocal