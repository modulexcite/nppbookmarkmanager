// This file is part of the Bookmark Manager plugin for Notepad++.
// Copyright (C) 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "PluginCommandList.h"

using namespace Npp::Plugin;

CPluginCommandList::CPluginCommandList()
  : mPluginCommands()
  , mShortcuts()
{}

void CPluginCommandList::clear()
{
  mPluginCommands.clear();
  mShortcuts.clear();
}

void CPluginCommandList::add(const std::basic_string<TCHAR> & aName, boost::optional<ShortcutKey> aShortcut,
  PFUNCPLUGINCMD aTrigger)
{
  FuncItem i;

  lstrcpy(i._itemName, aName.c_str());
  //i._cmdID = ;
  i._pFunc = aTrigger;
  i._init2Check = false;

  boost::shared_ptr<ShortcutKey> lShortcut;

  if (aShortcut.is_initialized())
    lShortcut.reset(new ShortcutKey(aShortcut.get()));

  mShortcuts.push_back(lShortcut);

  i._pShKey = mShortcuts.back().get();

  mPluginCommands.push_back(i);
}

const std::size_t CPluginCommandList::size() const
{
  return mPluginCommands.size();
}

const FuncItem * CPluginCommandList::getItems() const
{
  return !mPluginCommands.empty() ? &mPluginCommands[0] : 0;
}
