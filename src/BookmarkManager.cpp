// This file is part of the Bookmark Manager plugin for Notepad++.
// Copyright (C) 2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "BookmarkManager.h"
#include "PluginCommandList.h"
#include <lodepng.h>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>

using namespace Npp::Plugin;

CScintilla::CScintilla(const HWND aHandle, const NView aView)
  : mHandle(aHandle)
  , mView(aView)
{}

HWND CScintilla::getHandle() const
{
  return mHandle;
}

NView CScintilla::getView() const
{
  return mView;
}


int CScintilla::getCurrentLine() const
{
  int i(::SendMessage(mHandle, SCI_GETCURRENTPOS, 0, 0));

  return getLineFromPosition(i);
}

void CScintilla::removeMarkers(int aMarkerNumber)
{
  ::SendMessage(mHandle, SCI_MARKERDELETEALL, -1, 0);
}

bool CScintilla::doesLineHaveMarker(const int aLineNumber, const int aMarkerNumber) const
{
  int lResult(::SendMessage(mHandle, SCI_MARKERGET, aLineNumber, 0));

  return ((lResult >> aMarkerNumber) & 1) == 1;
}

bool CScintilla::addMarker(const int aLineNumber, const int aMarkerNumber, int & arMarkerHandle)
{
  if (!doesLineHaveMarker(aLineNumber, aMarkerNumber))
  {
    int lResult(::SendMessage(mHandle, SCI_MARKERADD, aLineNumber, aMarkerNumber));

    if (lResult != -1)
    {
      arMarkerHandle = lResult;

      return true;
    }
  }

  return false;
}

void CScintilla::removeMarker(const int aLineNumber, const int aMarkerNumber)
{
  ::SendMessage(mHandle, SCI_MARKERDELETE, aLineNumber, aMarkerNumber);
}

bool CScintilla::getLineFromMarker(const int aMarkerHandle, int & arLineNumber) const
{
  arLineNumber = ::SendMessage(mHandle, SCI_MARKERLINEFROMHANDLE, aMarkerHandle, 0);

  return arLineNumber != -1;
}

int CScintilla::getLineFromPosition(const int aPosition) const
{
  return ::SendMessage(mHandle, SCI_LINEFROMPOSITION, aPosition, 0);
}

int CScintilla::getMarginSymbols(const int aMarginIndex) const
{
  return ::SendMessage(mHandle, SCI_GETMARGINMASKN, aMarginIndex, 0);
}

void CScintilla::setMarginSymbols(const int aMarginIndex, const int aSymbolMask)
{
  ::SendMessage(mHandle, SCI_SETMARGINMASKN, aMarginIndex, aSymbolMask);
}

void CScintilla::setMarginType(const int aMarginIndex, const int aType)
{
  ::SendMessage(mHandle, SCI_SETMARGINTYPEN, aMarginIndex, aType);
}

void CScintilla::setMarginWidth(const int aMarginIndex, const int aWidth)
{
  ::SendMessage(mHandle, SCI_SETMARGINWIDTHN, aMarginIndex, aWidth);
}

int CScintilla::getLineCount() const
{
  return ::SendMessage(mHandle, SCI_GETLINECOUNT, 0, 0);
}

void CScintilla::gotoLine(const int aLineNumber, const bool aCenterCaretVertically)
{
  if (aCenterCaretVertically)
  {
    // note *1:
    //  unfortunately Scintilla API doesn't offer a way to request state of caret policy
    //  to revert new setting properly at the end of this functions

    ::SendMessage(mHandle, SCI_SETYCARETPOLICY, CARET_STRICT | CARET_EVEN, 0);
  }

  ::SendMessage(mHandle, SCI_GOTOLINE, aLineNumber, 0);

  if (aCenterCaretVertically)
    ::SendMessage(mHandle, SCI_SETYCARETPOLICY, 0 , 0); // simple revert, see *1
}

bool CScintilla::isLineVisible(const int aLineNumber)
{
  return ::SendMessage(mHandle, SCI_GETLINEVISIBLE, aLineNumber, 0);
}

void CScintilla::setCustomMarkerSymbol(const int aMarkerNumber, const unsigned aWidth, const unsigned aHeight,
  const CRGBAImage & aImage)
{
  ::SendMessage(mHandle, SCI_RGBAIMAGESETWIDTH, aWidth, 0);
  ::SendMessage(mHandle, SCI_RGBAIMAGESETHEIGHT, aHeight, 0);
  ::SendMessage(mHandle, SCI_MARKERDEFINERGBAIMAGE, aMarkerNumber, (LPARAM)&aImage[0]);
}

CBookmarkSymbol::CBookmarkSymbol(unsigned aWidth, unsigned aHeight, CRGBAImage aRGBAImage)
  : width(aWidth)
  , height(aHeight)
  , rgbaImage(aRGBAImage)
{}

CBookmark::CBookmark(const UCHAR aCharacter)
  : character(aCharacter)
  , symbol()
  , mMarkerNumberMap()
{}

bool CBookmark::getMarkerNumber(const CMarkerKey & aKey, int & arMarkerNumber) const
{
  CMarkerNumberMap::const_iterator i(mMarkerNumberMap.find(aKey));
  if (i != mMarkerNumberMap.end())
  {
    if (i->second.is_initialized())
    {
      arMarkerNumber = *i->second;

      return true;
    }
  }

  return false;
}

void CBookmark::setMarkerNumber(const CMarkerKey & aKey, const int aValue)
{
  mMarkerNumberMap[aKey] = aValue;
}

bool CBookmark::resetMarkerNumber(const CMarkerKey & aKey)
{
  return mMarkerNumberMap.erase(aKey) > 0;
}

void CBookmark::resetMarkerNumbers()
{
  mMarkerNumberMap.clear();
}

CMarker::CMarker()
 : handle(0)
 , symbolWidth(0)
{}

CMarker::CMarker(const int aHandle, const int aSymbolWidth)
 : handle(aHandle)
 , symbolWidth(aSymbolWidth)
{}

CBookmarkConfig::CBookmarkConfig(CPersistentConfigItem * apParent)
  : inherited(apParent, L"Bookmark")
  , mView(NView::Main)
  , mFilename(L"")
  , mCharacter(0)
  , mLine(0)
{}

CBookmarkConfig::CBookmarkConfig(const CBookmarkConfig & aSource)
  : inherited(aSource)
  , mView(aSource.mView)
  , mFilename(aSource.mFilename)
  , mCharacter(aSource.mCharacter)
  , mLine(aSource.mLine)
{}

CBookmarkConfig & CBookmarkConfig::operator= (const CBookmarkConfig & aSource)
{
  if (this == &aSource)
    return *this;

  beginUpdate();
  {
    inherited::operator=(aSource);

    setView(aSource.mView);
    setFilename(aSource.mFilename);
    setCharacter(aSource.mCharacter);
    setLine(aSource.mLine);
  }
  endUpdate();

  return *this;
}

void CBookmarkConfig::reset()
{
  beginUpdate();
  {
    inherited::reset();

    setView(NView::Main);
    setFilename(L"");
    setCharacter(0);
    setLine(0);
  }
  endUpdate();
}

const std::wstring & CBookmarkConfig::getFilename() const
{
  return mFilename;
}

NView CBookmarkConfig::getView() const
{
  return mView;
}

void CBookmarkConfig::setView(const NView aValue)
{
  if (aValue != mView)
  {
    mView = aValue;

    notifyChanged();
  }
}

void CBookmarkConfig::setFilename(const std::wstring & aValue)
{
  if (aValue != mFilename)
  {
    mFilename = aValue;

    notifyChanged();
  }
}

TCHAR CBookmarkConfig::getCharacter() const
{
  return mCharacter;
}

void CBookmarkConfig::setCharacter(const TCHAR aValue)
{
  if (aValue != mCharacter)
  {
    mCharacter = aValue;

    notifyChanged();
  }
}

unsigned CBookmarkConfig::getLine() const
{
  return mLine;
}

void CBookmarkConfig::setLine(const unsigned aValue)
{
  if (aValue != mLine)
  {
    mLine = aValue;

    notifyChanged();
  }
}

bool CBookmarkConfig::read(LosaXML::CDOMNode & arConfigNode, std::wstring & arErrorMessage)
{
  if (inherited::read(arConfigNode, arErrorMessage))
  {
    int lView(0);
    if (arConfigNode.getNodeValue(L"View", static_cast<int>(NView::Main), lView, arErrorMessage))
    {
      mView = static_cast<NView>(lView);

      if (arConfigNode.getNodeValue(L"Filename", L"", mFilename, arErrorMessage))
      {
        if (arConfigNode.getNodeValue(L"Character", 0, mCharacter, arErrorMessage))
        {
          if (arConfigNode.getNodeValue(L"Line", 0, mLine, arErrorMessage))
          {
            return true;
          }
        }
      }
    }
  }

  return false;
}

bool CBookmarkConfig::write(LosaXML::CDOMNode & arConfigNode, std::wstring & arErrorMessage)
{
  if (inherited::write(arConfigNode, arErrorMessage))
  {
    arConfigNode.addNode(L"View", static_cast<int>(mView));
    arConfigNode.addNode(L"Filename", mFilename);
    arConfigNode.addNode(L"Character", mCharacter);
    arConfigNode.addNode(L"Line", mLine);

    return true;
  }
  else
    return false;
}

CBookmarkConfigBunch::CBookmarkConfigBunch(CPersistentConfigItem * apParent, CBookmarkConfigBunchItemFactory & aItemFactory)
  : inherited(apParent, L"Bookmarks", aItemFactory)
{}

bool CBookmarkConfigBunch::get(const NView aView, const std::basic_string<TCHAR> & aFilename, CBookmarkConfigList & arItems) const
{
  arItems.clear();

  CBookmarkConfigList::const_iterator a, b;

  if (inherited::get(a, b))
  {
    while (a != b)
    {
      CBookmarkConfig * i(*a);

      if (i->getView() == aView && i->getFilename() == aFilename)
        arItems.push_back(i);

      ++a;
    }
  }

  return arItems.size() > 0;
}

bool CBookmarkConfigBunch::get(const NView aView, const std::basic_string<TCHAR> & aFilename, const TCHAR aCharacter,
  CBookmarkConfig * & aprItem) const
{
  aprItem = 0;

  CBookmarkConfigList::const_iterator a, b;

  if (inherited::get(a, b))
  {
    while (a != b)
    {
      CBookmarkConfig * i(*a);

      if (i->getView() == aView && i->getFilename() == aFilename && i->getCharacter() == aCharacter)
      {
        aprItem = i;

        break;
      }

      ++a;
    }
  }

  return aprItem;
}

bool CBookmarkConfigBunch::extract(CBookmarkConfig * aprItem)
{
  return inherited::extract(aprItem);
}

bool CBookmarkConfigBunch::add(CBookmarkConfig * apItem, std::wstring & arErrorMessage)
{
  return inherited::add(apItem, arErrorMessage);
}

bool CBookmarkConfigBunch::remove(CBookmarkConfig * & aprItem)
{
  return inherited::remove(aprItem);
}

CBookmarkManagerConfig::CBookmarkManagerConfig()
  : inherited(0, L"NppBookmarkManager")
  , mBookmarksItemFactory()
  , mBookmarks(this, mBookmarksItemFactory)
{}

CBookmarkManagerConfig::CBookmarkManagerConfig(const CBookmarkManagerConfig & aSource)
  : inherited(aSource)
  , mBookmarksItemFactory(aSource.mBookmarksItemFactory)
  , mBookmarks(this, mBookmarksItemFactory)
{
  mBookmarks = aSource.mBookmarks;
}

CBookmarkManagerConfig & CBookmarkManagerConfig::operator= (const CBookmarkManagerConfig & aSource)
{
  if (this == &aSource)
    return *this;

  beginUpdate();
  {
    inherited::operator=(aSource);

    mBookmarks = aSource.mBookmarks;
  }
  endUpdate();

  return *this;
}

CBookmarkConfigBunch & CBookmarkManagerConfig::getBookmarks()
{
  return mBookmarks;
}

bool CBookmarkManagerConfig::readFromFile(std::string aFilename, std::wstring & arErrorMessage) throw()
{
  boost::filesystem::path f(aFilename);

  if (boost::filesystem::exists(f))
    return inherited::readFromFile(aFilename, arErrorMessage);
  else
    return true; // non-existent config file is considered as default / empty config
}

bool CBookmarkManagerConfig::saveToFile(std::string aFilename, std::wstring & arErrorMessage) throw()
{
  boost::filesystem::path f(aFilename);

  if (boost::filesystem::exists(f.parent_path()) || // exists() call due to create_directories() returning false when f.parent_path() exists already
      boost::filesystem::create_directories(f.parent_path()))
    return inherited::saveToFile(aFilename, arErrorMessage);
  else
  {
    arErrorMessage = L"Cannot create directory " + std::wstring(aFilename.begin(), aFilename.end());

    return false;
  }
}

CMarkerKey::CMarkerKey()
  : mFilename()
  , mView(NView::Main)
{}

CMarkerKey::CMarkerKey(const std::basic_string<TCHAR> & aFilename, const NView aView)
  : mFilename(aFilename)
  , mView(aView)
{}

bool CMarkerKey::operator() (const CMarkerKey & a, const CMarkerKey & b) const
{
  // note: the following conditions ensure a strict weak ordering (see documentation of std::map)
  if (a.mFilename < b.mFilename)
    return true;

  if (b.mFilename < a.mFilename)
    return false;

  return a.mView < b.mView;
}

const std::basic_string<TCHAR> & CMarkerKey::getFilename() const
{
  return mFilename;
}

const NView CMarkerKey::getView() const
{
  return mView;
}

CBookmarkManager::CBookmarkManager(const NppData & aNppData, CPluginCommandList & aPluginCommands)
  : mNppData(aNppData)
  , mPluginCommands(aPluginCommands)
  , mBookmarkSymbolMarginIndex(1) // margin 1 is the bookmark margin in Notepad++
  , mBookmarkMap()
  , mMarkerMap()
  , mNppPluginDirectory(getNppPluginDirectory())
  , mConfigFilename(getConfigFilename())
  , mConfig()
{
  std::wstring e;
  if (!mConfig.readFromFile(mConfigFilename, e))
    ; // todo: add logging
}

CBookmarkManager::~CBookmarkManager()
{}

std::string CBookmarkManager::getConfigFilename() const
{

  std::basic_string<TCHAR> s(MAX_PATH, 0);
  if (::SendMessage(mNppData._nppHandle, NPPM_GETPLUGINSCONFIGDIR, MAX_PATH, (LPARAM)&s[0]))
  {
    s = std::basic_string<TCHAR>(&s[0]);

    std::string lResult(s.length(), 0);

    std::copy(s.begin(), s.end(), lResult.begin());

    lResult.append("/NppBookmarkManager/BookmarkManager.xml");

    return lResult;
  }
  else
    return "";
}

std::basic_string<TCHAR> CBookmarkManager::getNppPluginDirectory() const
{
  const int l(1024);
  TCHAR * b((TCHAR *)malloc(l * sizeof(TCHAR)));
  ::SendMessage(mNppData._nppHandle, NPPM_GETNPPDIRECTORY, l, (LPARAM)b);

  std::basic_string<TCHAR> s(b);
  s.append(TEXT("/plugins"));

  free(b);

  return s;
}

const boost::shared_ptr<CBookmark> CBookmarkManager::addBookmark(const UCHAR aBookmarkCharacter)
{
  boost::shared_ptr<CBookmark> lBookmark(new CBookmark(aBookmarkCharacter));

  // define custom pixmap for bookmark
  std::string lNppPluginDirectory(mNppPluginDirectory.begin(), mNppPluginDirectory.end());

  std::string lMarkerImageFilename(
    (boost::format("%s/images/NppBookmarkManager/Marker/%c.png") % lNppPluginDirectory.c_str() % lBookmark->character).str());

  CRGBAImage lRGBAImage;
  unsigned lWidth(0), lHeight(0);

  if (lodepng::decode(lRGBAImage, lWidth, lHeight, lMarkerImageFilename) == 0)
  {
    // SC_MARK_RGBAIMAGE
    lBookmark->symbol = CBookmarkSymbol(lWidth, lHeight, lRGBAImage);
  }
  else
    ; //SC_MARK_CIRCLE

  mBookmarkMap[aBookmarkCharacter] = lBookmark;

  return lBookmark;
}

void CBookmarkManager::addToggleCommand(const CBookmark & aBookmark, PFUNCPLUGINCMD apTrigger)
{
  std::basic_string<TCHAR> lCommandName(
    (boost::basic_format<TCHAR>(TEXT("Toggle Bookmark %s")) % std::basic_string<TCHAR>(1, aBookmark.character)).str());

  ShortcutKey sk;
  sk._isShift = true;
  sk._isCtrl = true;
  sk._isAlt = true;
  sk._key = aBookmark.character;

  mPluginCommands.add(lCommandName, sk, apTrigger);
}

void CBookmarkManager::addGotoCommand(const CBookmark & aBookmark, PFUNCPLUGINCMD apTrigger)
{
  std::basic_string<TCHAR> lCommandName(
    (boost::basic_format<TCHAR>(TEXT("Goto Bookmark %s")) % std::basic_string<TCHAR>(1, aBookmark.character)).str());

  ShortcutKey sk;
  sk._isShift = false;
  sk._isCtrl = true;
  sk._isAlt = true;
  sk._key = aBookmark.character;

  mPluginCommands.add(lCommandName, sk, apTrigger);
}

void CBookmarkManager::addClearBookmarkOfFileCommand(PFUNCPLUGINCMD apTrigger)
{
  ShortcutKey sk;
  sk._isShift = true;
  sk._isCtrl = true;
  sk._isAlt = true;
  sk._key = 'C';

  mPluginCommands.add(TEXT("Clear bookmarks of current file"), sk, apTrigger);
}

void CBookmarkManager::addClearBookmarkOfAllFilesCommand(PFUNCPLUGINCMD apTrigger)
{
  ShortcutKey sk;
  sk._isShift = true;
  sk._isCtrl = true;
  sk._isAlt = true;
  sk._key = 'A';

  mPluginCommands.add(TEXT("Clear bookmarks of all files"), sk, apTrigger);
}

bool CBookmarkManager::getCurrentScintilla(boost::scoped_ptr<CScintilla> & arCurrentScintilla) const
{
  int c(-1);

  ::SendMessage(mNppData._nppHandle, NPPM_GETCURRENTSCINTILLA, 0, (LPARAM)&c);

  if (c == 0)
  {
    arCurrentScintilla.reset(new CScintilla(mNppData._scintillaMainHandle, NView::Main));

    return true;
  }
  else
  if (c == 1)
  {
    arCurrentScintilla.reset(new CScintilla(mNppData._scintillaSecondHandle, NView::Second));

    return true;
  }
  else
    return false;
}

bool CBookmarkManager::getScintillaFromHandle(const HWND aScintillaHandle,
 boost::scoped_ptr<CScintilla> & arCurrentScintilla) const
{
  if (mNppData._scintillaMainHandle == aScintillaHandle)
  {
    arCurrentScintilla.reset(new CScintilla(aScintillaHandle, NView::Main));

    return true;
  }
  else
  if (mNppData._scintillaSecondHandle == aScintillaHandle)
  {
    arCurrentScintilla.reset(new CScintilla(aScintillaHandle, NView::Second));

    return true;
  }
  else
    return false;
}

void CBookmarkManager::setupBookmarkSymbolMargin(const CMarkerKey & aKey, CScintilla & aScintilla)
{
  int lMaxMarkerSymbolWidth(13);

 // CMarkerMap
 // mMarkerMap[aScintilla.getHandle()]
  CMarkerMap::const_iterator a(mMarkerMap.find(aKey));
  if (a != mMarkerMap.end())
  {
    for (CMarkerMap::mapped_type::const_iterator b(a->second.begin()); b != a->second.end(); ++b)
    {
      if (b->second.symbolWidth > lMaxMarkerSymbolWidth)
        lMaxMarkerSymbolWidth = b->second.symbolWidth;
    }
  }

  //aScintilla.setMarginType(mBookmarkSymbolMarginIndex, SC_MARGIN_SYMBOL);
  aScintilla.setMarginWidth(mBookmarkSymbolMarginIndex, lMaxMarkerSymbolWidth + 1);

  int aSymbolMask(aScintilla.getMarginSymbols(mBookmarkSymbolMarginIndex));
  // Notepad++ 6.7 bookmark margin symbol mask: 00000001110000000000000000000000


  // 1023 -> first 10 bits set to 1, each bit represents a logical marker number used by this plugin,
  // (the doc's of scintilla seem wrong here as it always mentions "symbols")
  aScintilla.setMarginSymbols(mBookmarkSymbolMarginIndex, aSymbolMask | 1023);
}

void CBookmarkManager::toggleBookmark(const UCHAR aBookmarkCharacter)
{
  boost::shared_ptr<CBookmark> lBookmark;

  if (getBookmark(aBookmarkCharacter, lBookmark))
  {
    // get current Scintilla
    boost::scoped_ptr<CScintilla> lScintilla(0);

    if (getCurrentScintilla(lScintilla))
    {
      int lBufferID(::SendMessage(mNppData._nppHandle, NPPM_GETCURRENTBUFFERID, 0, 0));
      std::basic_string<TCHAR> lFilename;

      if (getFilenameFromBuffer(mNppData._nppHandle, lBufferID, lFilename))
      {
        bool lAddMarker(true);
        int lCurrentLineNumber(lScintilla->getCurrentLine());

        // check whether marker has been assigned to a line already
        CMarkerKey k(lFilename, lScintilla->getView());
        int lMarkerNumber(0);

        if (lBookmark->getMarkerNumber(k, lMarkerNumber))
        {
          int lMarkerLineNumber(-1);

          removeMarker(k, lMarkerNumber, *lBookmark, *lScintilla, lMarkerLineNumber);

          if (lMarkerLineNumber == lCurrentLineNumber)
            lAddMarker = false;
        }

        if (lAddMarker)
        {
          if (addMarker(*lBookmark.get(), *lScintilla, lCurrentLineNumber, k))
          {
            // save bookmark in config
            CBookmarkConfig * lBookmarkConfig(new CBookmarkConfig(0));
            lBookmarkConfig->setView(lScintilla->getView());
            lBookmarkConfig->setCharacter(lBookmark->character);
            lBookmarkConfig->setFilename(lFilename);
            lBookmarkConfig->setLine(lCurrentLineNumber);

            std::wstring e;

            if (mConfig.getBookmarks().add(lBookmarkConfig, e))
            {
              if (!mConfig.saveToFile(mConfigFilename, e))
                ; // todo: add logging
            }
            else
              delete lBookmarkConfig;
          }
        }

        setupBookmarkSymbolMargin(k, *lScintilla);
      }
    }
  }
}

bool CBookmarkManager::removeMarker(const CMarkerKey & aKey, const int & aMarkerNumber, CBookmark & aBookmark,
  CScintilla & aScintilla, int & arMarkerLineNumber)
{
  // try to remove marker first as we want to achieve a toggle functionality
  bool lRemoveMarkerNumberFromMap(true);

  if (getLineFromMarker(aKey, aMarkerNumber, aScintilla, arMarkerLineNumber))
  {
    // check whether other bookmarks are associated with the marker...
    CBookmarkList lBookmarks;

    if (getBookmarkListFromMarker(aKey, aMarkerNumber, lBookmarks))
      if (lBookmarks.size() > 1)
        lRemoveMarkerNumberFromMap = false;
      else
      {
        // ... if not then remove the marker
        aScintilla.removeMarker(arMarkerLineNumber, aMarkerNumber);
      }

    // remove bookmark from config
    CBookmarkConfig * lBookmarkConfig(0);

    if (mConfig.getBookmarks().get(aKey.getView(), aKey.getFilename(), aBookmark.character, lBookmarkConfig))
    {
      mConfig.getBookmarks().remove(lBookmarkConfig);

      std::wstring e;

      if (!mConfig.saveToFile(mConfigFilename, e))
        ; // todo: add logging
    }
  }

  aBookmark.resetMarkerNumber(aKey);

  updateMarkerSymbol(aKey, aScintilla, aMarkerNumber);

  if (lRemoveMarkerNumberFromMap)
  {
    CMarkerMap::iterator i(mMarkerMap.find(aKey));
    if (i != mMarkerMap.end())
      i->second.erase(aMarkerNumber);
  }
}

bool CBookmarkManager::addMarker(CBookmark & aBookmark, CScintilla & aScintilla, int aLineNumber,
  const CMarkerKey & aKey)
{
  bool lResult(false);

  int lLineCount(aScintilla.getLineCount());
  // put bookmark to last line of it is beyond EOF
  if (aLineNumber > lLineCount - 1)
    aLineNumber = lLineCount - 1;

  int lNextFreeMarkerNumber(1);

  for (CBookmarkMap::const_iterator i(mBookmarkMap.begin()); i != mBookmarkMap.end(); ++i)
  {
    const CBookmark * lAnotherBookmark(i->second.get());

    if (lAnotherBookmark != &aBookmark)
    {
      int lAnotherMarkerNumber(0);

      if (lAnotherBookmark->getMarkerNumber(aKey, lAnotherMarkerNumber))
      {
        int lAnotherLineNumber(0);

        // check whether another bookmark is on the same line as bookmark that is going to be set
        if (getLineFromMarker(aKey, lAnotherMarkerNumber, aScintilla, lAnotherLineNumber))
          if (lAnotherLineNumber == aLineNumber)
          {
            // then we use the same marker number to display bookmarks next to each other
            lNextFreeMarkerNumber = lAnotherMarkerNumber;

            break;
          }

        if (lAnotherMarkerNumber >= lNextFreeMarkerNumber)
          lNextFreeMarkerNumber = lAnotherMarkerNumber + 1;
      }
    }
  }

  const int & lBookmarkMarkerNumber(lNextFreeMarkerNumber);

  aBookmark.setMarkerNumber(aKey, lBookmarkMarkerNumber);

  CMarker & lMarker(mMarkerMap[aKey][lBookmarkMarkerNumber]);

  if (aScintilla.doesLineHaveMarker(aLineNumber, lBookmarkMarkerNumber))
    lResult = true;
  else
  {
    int lMarkerHandle(0);

    if (aScintilla.addMarker(aLineNumber, lBookmarkMarkerNumber, lMarkerHandle))
    {
      lMarker.handle = lMarkerHandle;

      lResult = true;
    }
  }

  updateMarkerSymbol(aKey, aScintilla, lBookmarkMarkerNumber);

  return lResult;
}

void CBookmarkManager::gotoBookmark(const UCHAR aBookmarkCharacter)
{
  boost::shared_ptr<CBookmark> lBookmark;

  if (getBookmark(aBookmarkCharacter, lBookmark))
  {
    // get current Scintilla
    boost::scoped_ptr<CScintilla> lScintilla(0);

    if (getCurrentScintilla(lScintilla))
    {
      int lBufferID(::SendMessage(mNppData._nppHandle, NPPM_GETCURRENTBUFFERID, 0, 0));
      std::basic_string<TCHAR> lFilename;

      if (getFilenameFromBuffer(mNppData._nppHandle, lBufferID, lFilename))
      {
        CMarkerKey k(lFilename, lScintilla->getView());

        // check whether marker has been assigned to a line already
        int lMarkerNumber(0);

        if (lBookmark->getMarkerNumber(k, lMarkerNumber))
        {
          int lMarkerLineNumber(0);

          if (getLineFromMarker(k, lMarkerNumber, *lScintilla.get(), lMarkerLineNumber))
          {
            lScintilla->gotoLine(lMarkerLineNumber, true);

            if (!lScintilla->isLineVisible(lMarkerLineNumber))
              ::SendMessage(lScintilla->getHandle(), SCI_FOLDLINE, lMarkerLineNumber, SC_FOLDACTION_EXPAND);
          }
        }
      }
    }
  }
}

void CBookmarkManager::clearBookmarksOfCurrentFile()
{
  boost::scoped_ptr<CScintilla> lScintilla;

  if (getCurrentScintilla(lScintilla))
  {
    int lBufferID(::SendMessage(mNppData._nppHandle, NPPM_GETCURRENTBUFFERID, 0, 0));
    std::basic_string<TCHAR> lFilename;

    if (getFilenameFromBuffer(mNppData._nppHandle, lBufferID, lFilename))
    {
      CMarkerKey k(lFilename, lScintilla->getView());

      bool lSomethingRemoved(false);

      for (CBookmarkMap::value_type & p: mBookmarkMap)
      {
        boost::shared_ptr<CBookmark> & lBookmark(p.second);

        int lMarkerNumber(0);
        if (lBookmark->getMarkerNumber(k, lMarkerNumber))
        {
          int lMarkerLineNumber(-1);

          removeMarker(k, lMarkerNumber, *lBookmark.get(), *lScintilla, lMarkerLineNumber);

          lSomethingRemoved = true;
        }
      }

      if (lSomethingRemoved)
        setupBookmarkSymbolMargin(k, *lScintilla);
    }
  }
}

void CBookmarkManager::clearBookmarksOfAllFiles()
{
  // remove markers
  boost::scoped_ptr<CScintilla> lScintilla(0);

  if (getCurrentScintilla(lScintilla))
  {
    // first determine current filename
    int lBufferID(::SendMessage(mNppData._nppHandle, NPPM_GETCURRENTBUFFERID, 0, 0));
    std::basic_string<TCHAR> lFilename;

    if (getFilenameFromBuffer(mNppData._nppHandle, lBufferID, lFilename))
    {
      CMarkerKey lMarkerKeyOfCurrentFile(lFilename, lScintilla->getView());

      // then remove markers from all files by ...
      for (const CMarkerMap::value_type & a: mMarkerMap)
      {
        lFilename = a.first.getFilename();

        // ... switching to appropriate file ...
        if (lFilename.length() > 0 &&

          // TODO: handling of view is not working as WPARAM is not processed by Notepad++
          ::SendMessage(mNppData._nppHandle, NPPM_SWITCHTOFILE, (WPARAM)a.first.getView(), (LPARAM)&lFilename[0]))
        {
          // ... and remove markers
          for (const CMarkerMap::mapped_type::value_type & b: a.second)
            lScintilla->removeMarkers(b.first);
        }
      }

      mMarkerMap.clear();

      // reset bookmark - marker number association
      for (CBookmarkMap::value_type & p: mBookmarkMap)
        p.second->resetMarkerNumbers();

      // reset config
      mConfig.getBookmarks().reset();

      std::wstring e;

      if (!mConfig.saveToFile(mConfigFilename, e))
        ; // TODO: add logging

      // switch back to current file and update bookmark margin
      lFilename = lMarkerKeyOfCurrentFile.getFilename();

      if (lFilename.length() > 0 &&

          // TODO: handling of view is not working as WPARAM is not processed by Notepad++
          ::SendMessage(mNppData._nppHandle, NPPM_SWITCHTOFILE, (WPARAM)lMarkerKeyOfCurrentFile.getView(), (LPARAM)&lFilename[0]))
      {
        setupBookmarkSymbolMargin(lMarkerKeyOfCurrentFile, *lScintilla);
      }
    }
  }
}

bool CBookmarkManager::getBookmark(const UCHAR aBookmarkCharacter, boost::shared_ptr<CBookmark> & arBookmark) const
{
  CBookmarkMap::const_iterator i(mBookmarkMap.find(aBookmarkCharacter));
  if (i != mBookmarkMap.end())
  {
    arBookmark = i->second;

    return true;
  }
  else
    return false;
}

bool CBookmarkManager::getMarkerHandle(const CMarkerKey & aKey, const int aMarkerNumber, int & arMarkerHandle) const
{
  CMarkerMap::const_iterator a(mMarkerMap.find(aKey));
  if (a != mMarkerMap.end())
  {
    CMarkerMap::mapped_type::const_iterator b(a->second.find(aMarkerNumber));
    if (b != a->second.end())
    {
      arMarkerHandle = b->second.handle;

      return true;
    }
  }

  return false;
}

bool CBookmarkManager::getLineFromMarker(const CMarkerKey & aKey, const int aMarkerNumber, const CScintilla & aScintilla,
  int & arLineNumber) const
{
  int lMarkerHandle(0);

  if (getMarkerHandle(aKey, aMarkerNumber, lMarkerHandle))
    if (aScintilla.getLineFromMarker(lMarkerHandle, arLineNumber))
      return true;

  return false;
}

bool CBookmarkManager::getBookmarkListFromMarker(const CMarkerKey & aKey, const int aMarkerNumber,
  CBookmarkList & arBookmarks) const
{
  arBookmarks.clear();

  for (CBookmarkMap::const_iterator i(mBookmarkMap.begin()); i != mBookmarkMap.end(); ++i)
  {
    const CBookmark * lBookmark(i->second.get());

    int lMarkerNumber(0);

    if (lBookmark->getMarkerNumber(aKey, lMarkerNumber) && lMarkerNumber == aMarkerNumber)
      arBookmarks.push_back(lBookmark);
  }

  return arBookmarks.size() > 0;
}

bool CBookmarkManager::createMarkerSymbol(const CMarkerKey & aKey, const int aMarkerNumber, unsigned & aWidth,
  unsigned & aHeight, CRGBAImage & arBookmarkImage) const
{
  aWidth = 0;
  aHeight = 0;
  arBookmarkImage.clear();

  unsigned lMarkerSymbolGapSize(1);

  // find bookmarks associated with the given marker number
  typedef std::list<const CBookmarkSymbol *> CMarkerSymbols;
  CMarkerSymbols lMarkerSymbols;
  CBookmarkList lBookmarks;

  if (getBookmarkListFromMarker(aKey, aMarkerNumber, lBookmarks))
  {
    for (CBookmarkList::const_iterator i(lBookmarks.begin()); i != lBookmarks.end(); ++i)
    {
      const CBookmark * lBookmark(*i);

      if (lBookmark->symbol.is_initialized())
      {
        aWidth += lBookmark->symbol->width + lMarkerSymbolGapSize;

        if (lBookmark->symbol->height > aHeight)
          aHeight = lBookmark->symbol->height;

        lMarkerSymbols.push_back(lBookmark->symbol.get_ptr());
      }
    }

    if (aWidth >= lMarkerSymbolGapSize)
      aWidth -= lMarkerSymbolGapSize;

    // prepare bookmark image
    unsigned lBookmarkImageScanSize(aWidth * 4);

    arBookmarkImage.resize(lBookmarkImageScanSize * aHeight, 0);

    // render bookmark image
    unsigned lOffsetX(0);

    for (CMarkerSymbols::const_iterator s(lMarkerSymbols.begin()); s != lMarkerSymbols.end(); ++s)
    {
      const CBookmarkSymbol * lMarkerSymbol(*s);
      const CRGBAImage & lMarkerSymbolImage(lMarkerSymbol->rgbaImage);
      unsigned lMarkerSymbolImageScanSize(lMarkerSymbol->width * 4);

      CRGBAImage::iterator a(arBookmarkImage.begin() + lOffsetX);

      for (CRGBAImage::const_iterator b(lMarkerSymbolImage.begin()); b != lMarkerSymbolImage.end();
        b += lMarkerSymbolImageScanSize)
      {
        std::copy(b, b + lMarkerSymbolImageScanSize, a);

        a += lBookmarkImageScanSize;
      }

      lOffsetX += lMarkerSymbolImageScanSize + lMarkerSymbolGapSize * 4;
    }
  }

  return arBookmarkImage.size() > 0;
}

void CBookmarkManager::updateMarkerSymbol(const CMarkerKey & aKey, CScintilla & aScintilla, const int aMarkerNumber)
{
  int lMarkerSymbolKind(SC_MARK_CIRCLE);
  unsigned lMarkerSymbolWidth(0), llMarkerSymbolHeight(0);
  CRGBAImage lMarkerSymbolImage;

  if (createMarkerSymbol(aKey, aMarkerNumber, lMarkerSymbolWidth, llMarkerSymbolHeight, lMarkerSymbolImage))
  {
    aScintilla.setCustomMarkerSymbol(aMarkerNumber, lMarkerSymbolWidth, llMarkerSymbolHeight, lMarkerSymbolImage);

    lMarkerSymbolKind = SC_MARK_RGBAIMAGE;
  }
  else
    lMarkerSymbolKind = SC_MARK_CIRCLE;

  CMarkerMap::iterator a(mMarkerMap.find(aKey));
  if (a != mMarkerMap.end())
  {
    CMarkerMap::mapped_type::iterator b(a->second.find(aMarkerNumber));
    if (b != a->second.end())
      b->second.symbolWidth = lMarkerSymbolWidth;
  }

  ::SendMessage(aScintilla.getHandle(), SCI_MARKERDEFINE, aMarkerNumber, lMarkerSymbolKind);
}

void CBookmarkManager::textDeleted(const HWND aScintillaHandle, const int aPosition, const int aLinesAdded)
{
  // check whether lines were deleted
  if (aLinesAdded < 0)
  {
    boost::scoped_ptr<CScintilla> lScintilla;

    if (getScintillaFromHandle(aScintillaHandle, lScintilla))
    {
      int lCurrentLineNumber(lScintilla->getLineFromPosition(aPosition));

      int lBufferID(::SendMessage(mNppData._nppHandle, NPPM_GETCURRENTBUFFERID, 0, 0));
      std::basic_string<TCHAR> lFilename;

      if (getFilenameFromBuffer(mNppData._nppHandle, lBufferID, lFilename))
      {
        CMarkerKey k(lFilename, lScintilla->getView());

        bool lSomethingUpdated(false);

        // find all bookmarks merged to the current line by the Scintilla delete operation
        for (CBookmarkMap::iterator i(mBookmarkMap.begin()); i != mBookmarkMap.end(); ++i)
        {
          boost::shared_ptr<CBookmark> & lBookmark(i->second);

          int lMarkerNumber(0);

          if (lBookmark->getMarkerNumber(k, lMarkerNumber))
          {
            int lMarkerLineNumber(0);

            if (getLineFromMarker(k, lMarkerNumber, *lScintilla, lMarkerLineNumber))
            {
              if (lMarkerLineNumber == lCurrentLineNumber)
              {
                // its a merged bookmark thus remove bookmark to free marker and...
                removeMarker(k, lMarkerNumber, *lBookmark, *lScintilla, lMarkerLineNumber);

                // ...to add it again which ensures that a new marker is assigned and
                // thus the merged bookmarks will be displayed next to each other again
                toggleBookmark(lBookmark->character);

                lSomethingUpdated = true;
              }
            }
          }
        }

        if (lSomethingUpdated)
          setupBookmarkSymbolMargin(k, *lScintilla);
      }
    }
  }
}

bool CBookmarkManager::getFilenameFromBuffer(const HWND aNppHandle, const int aBufferID,
  std::basic_string<TCHAR> & arFilename) const
{
  int lBufferSize(::SendMessage(aNppHandle, NPPM_GETFULLPATHFROMBUFFERID, aBufferID, 0));

  if (lBufferSize > 0)
  {
    arFilename.resize(lBufferSize + 1, 0);

    int lFilenameLength(::SendMessage(aNppHandle, NPPM_GETFULLPATHFROMBUFFERID, aBufferID, (LPARAM)&arFilename[0]));

    if (lFilenameLength > 0)
    {
      arFilename.resize(lFilenameLength);

      return true;
    }
  }

  return false;
}

void CBookmarkManager::fileOpened(const std::basic_string<TCHAR> & aFilename)
{
  // restore bookmarks
  boost::scoped_ptr<CScintilla> lScintilla(0);

  if (getCurrentScintilla(lScintilla))
  {
    if (aFilename.length() > 0 && ::SendMessage(mNppData._nppHandle, NPPM_SWITCHTOFILE, (WPARAM)lScintilla->getView(), (LPARAM)&aFilename[0]))
    {
      CBookmarkConfigList lBookmarkConfigList;

      if (mConfig.getBookmarks().get(lScintilla->getView(), aFilename, lBookmarkConfigList))
      {
        CMarkerKey k(aFilename, lScintilla->getView());

        for (CBookmarkConfig * lBookmarkConfig: lBookmarkConfigList)
        {
          boost::shared_ptr<CBookmark> lBookmark;

          if (getBookmark(lBookmarkConfig->getCharacter(), lBookmark))
          {
            if (addMarker(*lBookmark.get(), *lScintilla, lBookmarkConfig->getLine(), k))
              ; // nothing to do yet
          }
        }

        setupBookmarkSymbolMargin(k, *lScintilla);
      }
    }
  }
}

void CBookmarkManager::fileBeforeClose(const HWND aScintillaHandle, const std::basic_string<TCHAR> & aFilename)
{}

void CBookmarkManager::fileChanged(const std::basic_string<TCHAR> & aFilename)
{
  // adjust bookmark margin
  boost::scoped_ptr<CScintilla> lScintilla(0);

  if (getCurrentScintilla(lScintilla))
  {
    CMarkerKey k(aFilename, lScintilla->getView());

    CMarkerMap::const_iterator a(mMarkerMap.find(k));
    if (a != mMarkerMap.end())
    {
      for (const CMarkerMap::mapped_type::value_type & b: a->second)
      {
        const int & lMarkerNumber(b.first);

        updateMarkerSymbol(k, *lScintilla, lMarkerNumber);
      }
    }

    setupBookmarkSymbolMargin(k, *lScintilla);
  }
}
