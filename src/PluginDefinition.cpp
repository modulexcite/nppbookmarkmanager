//this file is part of notepad++
//Copyright (C)2003 Don HO <donho@altern.org>
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either
//version 2 of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include "PluginDefinition.h"
#include "PluginCommandList.h"
#include "BookmarkManager.h"
#include <boost/shared_ptr.hpp>

//
// The plugin data that Notepad++ needs
//

//
// The data of Notepad++ that you can use in your plugin commands
//
NppData nppData;
Npp::Plugin::CPluginCommandList pcl;
Npp::Plugin::CBookmarkManager * bm(0);

//
// Initialize your plugin data here
// It will be called while plugin loading
void pluginInit(HANDLE hModule)
{
}

//
// Here you can do the clean up, save the parameters (if any) for the next session
//
void pluginCleanUp()
{
  if (bm)
  {
    delete bm;
    bm = 0;
  }
}

//
// Initialization of your plugin commands
// You should fill your plugins commands here

void commandMenuInit()
{
  // initialize bm here as values of nppData are not initialized before this function is called
  if (!bm)
    bm = new Npp::Plugin::CBookmarkManager(nppData, pcl);

  const boost::shared_ptr<Npp::Plugin::CBookmark> b1(bm->addBookmark('1')),
    b2(bm->addBookmark('2')),
    b3(bm->addBookmark('3')),
    b4(bm->addBookmark('4')),
    b5(bm->addBookmark('5')),
    b6(bm->addBookmark('6')),
    b7(bm->addBookmark('7')),
    b8(bm->addBookmark('8')),
    b9(bm->addBookmark('9'));

  bm->addToggleCommand(*b1, commandMenuToggleBookmark1);
  bm->addToggleCommand(*b2, commandMenuToggleBookmark2);
  bm->addToggleCommand(*b3, commandMenuToggleBookmark3);
  bm->addToggleCommand(*b4, commandMenuToggleBookmark4);
  bm->addToggleCommand(*b5, commandMenuToggleBookmark5);
  bm->addToggleCommand(*b6, commandMenuToggleBookmark6);
  bm->addToggleCommand(*b7, commandMenuToggleBookmark7);
  bm->addToggleCommand(*b8, commandMenuToggleBookmark8);
  bm->addToggleCommand(*b9, commandMenuToggleBookmark9);

  bm->addGotoCommand(*b1, commandMenuGotoBookmark1);
  bm->addGotoCommand(*b2, commandMenuGotoBookmark2);
  bm->addGotoCommand(*b3, commandMenuGotoBookmark3);
  bm->addGotoCommand(*b4, commandMenuGotoBookmark4);
  bm->addGotoCommand(*b5, commandMenuGotoBookmark5);
  bm->addGotoCommand(*b6, commandMenuGotoBookmark6);
  bm->addGotoCommand(*b7, commandMenuGotoBookmark7);
  bm->addGotoCommand(*b8, commandMenuGotoBookmark8);
  bm->addGotoCommand(*b9, commandMenuGotoBookmark9);

  bm->addClearBookmarkOfFileCommand(commandMenuClearBookmarksOfCurrentFile);
  bm->addClearBookmarkOfAllFilesCommand(commandMenuClearBookmarksOfAllFiles);

  pcl.add(TEXT("About"), boost::none, showAboutDialog);
}

//
// Here you can do the clean up (especially for the shortcut)
//
void commandMenuCleanUp()
{
  pcl.clear();
}

//----------------------------------------------//
//-- STEP 4. DEFINE YOUR ASSOCIATED FUNCTIONS --//
//----------------------------------------------//

void commandMenuToggleBookmark0()
{
  bm->toggleBookmark(48);
}

void commandMenuToggleBookmark1()
{
  bm->toggleBookmark(49);
}

void commandMenuToggleBookmark2()
{
  bm->toggleBookmark(50);
}

void commandMenuToggleBookmark3()
{
  bm->toggleBookmark(51);
}

void commandMenuToggleBookmark4()
{
  bm->toggleBookmark(52);
}

void commandMenuToggleBookmark5()
{
  bm->toggleBookmark(53);
}

void commandMenuToggleBookmark6()
{
  bm->toggleBookmark(54);
}

void commandMenuToggleBookmark7()
{
  bm->toggleBookmark(55);
}

void commandMenuToggleBookmark8()
{
  bm->toggleBookmark(56);
}

void commandMenuToggleBookmark9()
{
  bm->toggleBookmark(57);
}

void commandMenuGotoBookmark0()
{
  bm->gotoBookmark(48);
}

void commandMenuGotoBookmark1()
{
  bm->gotoBookmark(49);
}

void commandMenuGotoBookmark2()
{
  bm->gotoBookmark(50);
}

void commandMenuGotoBookmark3()
{
  bm->gotoBookmark(51);
}

void commandMenuGotoBookmark4()
{
  bm->gotoBookmark(52);
}

void commandMenuGotoBookmark5()
{
  bm->gotoBookmark(53);
}

void commandMenuGotoBookmark6()
{
  bm->gotoBookmark(54);
}

void commandMenuGotoBookmark7()
{
  bm->gotoBookmark(55);
}

void commandMenuGotoBookmark8()
{
  bm->gotoBookmark(56);
}

void commandMenuGotoBookmark9()
{
  bm->gotoBookmark(57);
}

void commandMenuClearBookmarksOfCurrentFile()
{
  bm->clearBookmarksOfCurrentFile();
}

void commandMenuClearBookmarksOfAllFiles()
{
  bm->clearBookmarksOfAllFiles();
}

void showAboutDialog()
{
  std::basic_string<TCHAR> lCaption(TEXT("About "));
  lCaption.append(TEXT(PROJECT_DISPLAY_NAME));

  std::basic_string<TCHAR> lText;
  lText.append(TEXT("Version "));
  lText.append(TEXT(PROJECT_VERSION));
  lText.append(TEXT("\r\n\r\n"));
  lText.append(TEXT("Copyright (C) "));
  lText.append(TEXT(PROJECT_LEGAL_COPYRIGHT));
  lText.append(TEXT("\r\n"));
  lText.append(TEXT("This program comes with ABSOLUTELY NO WARRANTY; for details see License.txt.\r\n\r\n"));
  lText.append(TEXT(PROJECT_COMMENTS));

  ::MessageBox(nppData._nppHandle, lText.c_str(), lCaption.c_str(), 0);
}
