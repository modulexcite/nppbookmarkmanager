cmake_minimum_required(VERSION 2.8.12)
project(NppBookmarkManager)

# extend search path for find_package()
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "$ENV{CMAKE_EXTENDED_MODULE_PATH}")

find_package(Boost 1.57.0 REQUIRED COMPONENTS system filesystem)
find_package(LodePNG REQUIRED)
find_package(LosaXML 0.1.1.0 REQUIRED)

# increment build number
include(CMakeBuild.txt)
MATH(EXPR PROJECT_BUILD_NUMBER "${PROJECT_BUILD_NUMBER}+1")
message("-- Build number: ${PROJECT_BUILD_NUMBER}")
file(WRITE CMakeBuild.txt "set(PROJECT_BUILD_NUMBER ${PROJECT_BUILD_NUMBER})")

# project info
set(PROJECT_DISPLAY_NAME "Bookmark Manager")
set(PROJECT_LEGAL_COPYRIGHT "2015 ViRuSTriNiTy <cradle-of-mail at gmx.de>")
set(PROJECT_COMMENTS "This plugin uses the following libraries: \\r\\nLodePNG <http://lodev.org/lodepng>\\r\\nLosaXML <https://bitbucket.org/ViRuSTriNiTy/losaxml>")
set(PROJECT_VERSION_MAJOR 0)
set(PROJECT_VERSION_MINOR 1)
set(PROJECT_VERSION_BUILD ${PROJECT_BUILD_NUMBER})

configure_file(src/Version.rc obj/Version.rc @ONLY)
configure_file(src/Version.h obj/Version.h @ONLY)

# enable C++ 11
if (MINGW OR MSYS)
  add_compile_options(-std=c++0x)
endif()

#enable static linking
if (MINGW OR MSYS)
  set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -static-libgcc -static-libstdc++")
else()
  message(WARNING "Project will be compiled without static linking configuration")
endif()

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY bin)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY obj)

add_definitions(-DUNICODE)

include_directories(
  ${Boost_INCLUDE_DIRS}
  ${LodePNG_INCLUDE_DIRS}
  ${LosaXML_INCLUDE_DIRS}
)

set(SOURCES src/PluginDefinition.cpp
  src/NppPluginDemo.cpp
  src/BookmarkManager.cpp
  src/PluginCommandList.cpp
)

link_directories(
  ${Boost_LIBRARY_DIRS}
  ${LodePNG_LIBRARY_DIRS}
  ${LosaXML_LIBRARY_DIRS}
)

add_library(${PROJECT_NAME} SHARED ${SOURCES} obj/Version.rc)

# update modified timestamp of CMakeBuild.txt after building library, 
# this ensures that CMakeList.txt is parsed again before next build 
# as CMakeBuild.txt is an "include dependency" and thus build number 
# is incremened by CMake continiously
add_custom_command(TARGET ${PROJECT_NAME}
  POST_BUILD
  COMMAND COMMAND cmake -E touch CMakeBuild.txt
)

target_link_libraries(${PROJECT_NAME}
  ${Boost_LIBRARIES}
  ${LodePNG_LIBRARIES}
  ${LosaXML_LIBRARIES}
)

set_target_properties(${PROJECT_NAME}
  PROPERTIES
    PREFIX ""
    VERSION "${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}"
    SOVERSION "${PROJECT_VERSION_BUILD}"
)

file(COPY License.txt README.md DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/bin/NppBookmarkManager)

set(NppPluginDirectory "$ENV{NPP_ROOT}/plugins")

if(IS_DIRECTORY "${NppPluginDirectory}")
  install(TARGETS ${PROJECT_NAME} RUNTIME DESTINATION ${NppPluginDirectory})
  
  install(
    FILES 
      bin/images/NppBookmarkManager/Marker/1.png
      bin/images/NppBookmarkManager/Marker/2.png
      bin/images/NppBookmarkManager/Marker/3.png
      bin/images/NppBookmarkManager/Marker/4.png
      bin/images/NppBookmarkManager/Marker/5.png
      bin/images/NppBookmarkManager/Marker/6.png
      bin/images/NppBookmarkManager/Marker/7.png
      bin/images/NppBookmarkManager/Marker/8.png
      bin/images/NppBookmarkManager/Marker/9.png
    DESTINATION 
      "${NppPluginDirectory}/images/NppBookmarkManager/Marker")

  install(
    FILES 
      License.txt README.md
    DESTINATION 
      "${NppPluginDirectory}/NppBookmarkManager")
endif()